@ECHO OFF
REM get wav file path and extension file path, passed as command line args
set extpath=%1
set wavpath=%2
"%extpath%\ConsoleTool.exe" /HIDE
echo "extpath:" %extpath%
echo "wav path:" %wavpath%

REM  remove marker indicating to extension that wav file creation has completed
del %extpath%\wavdone.txt
del %extpath%\nowav.txt

REM  encode the existing wave as wav file
copy %extpath%\speak.wav %wavpath%

set error=%ERRORLEVEL%
echo "wave error: %error%"

    IF %ERRORLEVEL% NEQ 0 (
    echo "Could not copy wave file"
    REM create marker file
    echo "copy error" > %extpath%\nowav.txt
    ) ELSE (
    REM completed marker file
    echo "OK"
    echo "wav done" > %extpath%\wavdone.txt
    )
