#!/bin/sh

# Remove marker files
extpath=$1
echo "extpath: " $extpath

echo "Deleting marker files..." > $extpath/delResult.txt

rm -v $extpath/oggdone.txt 2>>$extpath/delResult.txt
rm -v $extpath/noogg.txt 2>>$extpath/delResult.txt
rm -v $extpath/mp3done.txt 2>>$extpath/delResult.txt
rm -v $extpath/nomp3.txt 2>>$extpath/delResult.txt
rm -v $extpath/wavdone.txt 2>>$extpath/delResult.txt
rm -v $extpath/nowav.txt 2>>$extpath/delResult.txt
