#!/bin/bash
if pgrep "orca" > /dev/null; then
	killall orca &
else 
	orca --replace &
fi
exit 0