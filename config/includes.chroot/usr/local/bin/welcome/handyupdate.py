#!/usr/bin/env python3
# -*-coding:utf-8-*

from gi.repository import Gtk
import os
import locale
LOCALE = locale.setlocale(locale.LC_ALL, "")[0:2]

class HandyUpdate():

    def on_button1_clicked(self, widget):
        Gtk.main_quit()

    def on_button2_clicked(self, widget):
        os.system("handy-update-checker &")
        Gtk.main_quit()
		
    def __init__(self):
        window = Gtk.Window()
        window.set_title("HandyUpdate")
        window.connect("destroy", self.on_button1_clicked)
        window.connect("delete_event", lambda x,y: Gtk.main_quit())
        window.set_default_size(300, 200)

        vBox = Gtk.VBox()
        label = Gtk.Label()
        if LOCALE == 'fr':
            label.set_text("\n    Vous venez d'installer HandyLinux ... merci :)    \n" +
                            "\n    Votre système est prêt à être utilisé, cependant    \n" +
                            "    selon la date de construction de votre image ISO,    \n" +
                            "    des mises à jour peuvent être disponibles.    \n" +
                            "\n    Désirez-vous vérifier les mises à jour disponibles ?    \n" +
                            "\n    Une connexion internet active est nécessaire.    \n" +
                            "    Votre mot de passe vous sera demandé.    \n")
        else:
            label.set_text("\n    You have just installed HandyLinux ... merci :)    \n" +
						"\n         Your system is ready to use, but              \n" +
						"       depending on your ISO building date,            \n" +
						"          some updates may be available.               \n" +
						"\n    Do you want to check for available update(s) ?    \n" +
						"\n         You need an active network.    \n" +
						"         Your password will be asked.    \n")
            
        vBox.pack_start(label, False, False, 10)

        hBox = Gtk.HBox()
        if LOCALE == 'fr':
            bouton1 = Gtk.ToggleButton(label = "non merci")
        else:
            bouton1 = Gtk.ToggleButton(label = "no thanks")
        bouton1.connect("clicked", self.on_button1_clicked)
        if LOCALE == 'fr':
            bouton2 = Gtk.ToggleButton(label = "oui, merci")
        else:
            bouton2 = Gtk.ToggleButton(label = "yes, thanks")
        bouton2.connect("clicked", self.on_button2_clicked)
        hBox.add(bouton1)
        hBox.add(bouton2)

        vBox.pack_end(hBox,  False, False, 4)
        window.add(vBox)
        window.show_all()
        Gtk.main()
		
if __name__ == '__main__':
	HandyUpdate()
