#!/bin/bash
# post-install script for handylinux :
#  - fix sources.list
#  - set installation version and date
#  - purge fr pkgs/files
#  - set nice handy grub theme
#  - add beep to GRUB
#  - activate lightdm accessibility
#  - fix welcome launcher after installation
#  - fix usb installation entry in fstab (thx to Corenominal)
# arpinux <arpinux@member.fsf.org>
#############################################################

# set handylinux sources.list
mv -f /usr/share/handylinux/sources.list /etc/apt/sources.list

# set handylinux installation date and initial release"
echo "HandyLinux-2.5 `date +%a\ %d\ %b\ %Y`" > /etc/handylinux_installation

# clean language
# purge french packages
dpkg --purge libreoffice-l10n-fr libreoffice-help-fr icedove-l10n-fr firefox-l10n-fr
dpkg --purge hunspell-fr manpages-fr manpages-fr-extra debian-reference-fr
# purge french doc
dpkg --purge handylinux-doc-fr
# purge sudo password trad
rm /etc/sudoers.d/hl-sudoers-fr

# ajouter un background à GRUB
echo "GRUB_BACKGROUND=\"/usr/share/images/grub/handylinux.tga\"" >> /etc/default/grub

# identification de l'installation avec synth-se vocale
echo "installation vocale" > /usr/share/handylinux/installsynth

# ajouter un beep à GRUB
echo "GRUB_INIT_TUNE=\"480 440 1\"" >> /etc/default/grub

# accessibilé de lightdm
mkdir -p /etc/xdg/lightdm/lightdm.conf.d
echo "[SeatDefaults]\ngreeter-wrapper=/usr/bin/orca-dm-wrapper" >> /etc/xdg/lightdm/lightdm.conf.d/80_orca-dm-wrapper.conf

# prise en compte du fond pour GRUB et du noyau conservé
update-grub

# changement du lanceur d'accueil pour les utilisateurs supplémentaires
sed -i s/me.sh/me2.sh/g /etc/skel/.config/autostart/welcome.desktop

# This is a fugly hack for fixing fstab after installing
# CrunchBang using unetbootin. Basically, if using unetbootin,
# the USB device is identified as a cdrom drive and added to
# /etc/fstab as that. This script will find any such entries
# in fstab and disable them. It is looking for cdrom entries
# which reference devices under "/dev/sdX".

# a big thank you to corenominal, dev of CrunchbangLinux

FSTAB=/etc/fstab
if grep "^/dev/sd" ${FSTAB} | grep "/media/" | grep "auto"
then
    NEEDLE=`grep "^/dev/sd" ${FSTAB} | grep "/media/" | grep "auto"`
    if ! echo "${NEEDLE}" | grep "#/dev/sd"
    then
        CORK="#${NEEDLE}"
        rpl -q "${NEEDLE}" "${CORK}" ${FSTAB}
    fi
fi
exit 0
