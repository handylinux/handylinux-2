HOW TO maintenir les paquets et le dépôt HandyLinux
====================================================

*note : cette documentation est spécifique aux dépôts handylinux et devra être adaptée pour une autre dérivée Debian.*  
*cette méthode n'est que la mienne ne tient pas lieu de méthode absolue pour la maintenance des paquets Debian. (arpinux)*

pour une documentation exhaustive, vous pouvez consulter [le guide Complet Debian](https://www.debian.org/doc/manuals/maint-guide/dreq.fr.html)

Les contraintes du serveur
--------------------------
HandyLinux ne dispose pas de serveur dédié car nous n'avons pas de bénévoles adminserv pour s'en occuper à plein temps :)  
donc on travaille avec un serveur mutualisé OVH qui ne nous permet pas d'installer les outils nécessaires à la maintenance d'un dépôt.  
c'est pourquoi le dépôt HandyLinux est intégralement travaillé en local puis uploadé par ftp à chaque mise à jour.  
cela permet aussi de tester en local tous les changements avant de les faire suivre sur le dépôt en ligne.

HandyLinux est une Debian-Derivative, ce qui implique de fournir les sources des paquets du dépôt spécifique (sauf pour les paquets 'compiz' uploadés en .deb uniquement).  
la présence des sources dans le dépôt implique un certain processus pour l'intégration des paquets.

Les outils utilisés
-------------------
l'ajout et la mise à jour des paquets se fait grâce à **reprepro**  
la construction des paquets se fait grâce à l'outil **debuild** du paquet **devscripts** (le paquet magique du mainteneur) et **debhelper**.

Environnement de dev
====================
bien sûr, chacun travaille comme il veut :) ceci n'est qu'une façon de faire, pour filer une vue d'ensemble.  

- un dossier pour tout faire : dev des paquets et dépôt. de cette manière, une sauvegarde régulière de ce dossier permettra la sauvegarde de votre travail ET du dépôt de le distro
- une sauvegarde automatique du dépôt en place et fonctionnel : afin de ne jamais planter le dépôt, il faut travailler sur le dépôt, puis le tester en local avec vbox ou un ordi test.  
une fois ces tests concluants, vous pouvez uploader le contenu du dépôt local, supprimer l'archive dépôt.txz puis la recréer avec le nouveau dépôt fraîchement mis en ligne.  
de cette manière, qu'il y ait une erreur lors de l'intégration ou lors de l'upload, vous conservez une archive dépôt.txz fonctionnelle à remettre en ligne en urgence ;)

! en cas de doute sur le dépôt en ligne, mieux vaut le supprimer de suite et restaurer l'archive de sauvegarde !  
l'injoignabilité est préférable à l'instabilité :)

structure de l'environnement de dev handylinux
----------------------------------------------

	└──/handydeb/            : dossier de développement handylinux
	    ├──/paquet1          : dossier du paquet1.
	    │   ├──/archives/    : dossier contenant les anciennes versions du paquet1.
	    │   └──/paquet1-1.0/ : dossier contenant les sources du paquet1 en version 1.0 (la structure d'un paquet sera détaillée plus tard).
	    ├──/handydeb/paquet2 : dossier du paquet2.
	    .
	    ├──/repo             : dépôt handylinux (la structure sera détaillée plus tard).
	    └──/repo.txz         : archive sauvegarde du dépôt opérationnel en ligne.

utilisation de pbuilder
-----------------------
pour être 100% Debian, il faudrait utiliser pbuilder pour le développement et la construction des paquets.  
c'est la méthode normée pour être sûr de choper les bonnes dépendances etc. bref. étant donné que le développement se fait sur une Debian du même type qu'HandyLinux et que tous les nouveaux paquets à maintenir sont en 'all.deb', nul besoin de pbuilder :)

Création d'un paquet
====================
processus de création d'un paquet *from scratch*  

astuce : avant de nommer un paquet ou un script, prenez soin d'aller vérifier [dans les paquets Debian](https://www.debian.org/distrib/packages) si le nom n'est pas réservé.  

astuce : lors du dh_make pour créer un paquet ou tout autre opération devscript, on peut préciser le nom et le mail du mainteneur par défaut dans le bashrc/zshrc :  

	export DEBFULLNAME="prénom nom"
	export DEBEMAIL="adresse mail"

note: l'adresse renseignée ici sera celle utilisée pour la signature du paquet et lors de l'intégration dans le dépôt.  

astuce signature/authentification :  
pour signer ses paquets et le dépôts, il faut bien sûr un clé GPG active (utilisez 'gpg-agent --deamon en début de session') et associée au mail pris en exemple ci-dessus.  
pour vérifier votre clé, `gpg --list-keys` . repérez la clé associé à l'adresse renseignée dans votre bashrc/zshrc puis indiquez-la dans votre ~/.gnupg/gpg.conf :  

	# If you have more than 1 secret key in your keyring, you may want to
	# uncomment the following option and set your preferred keyid.
	
	default-key XXXXXXXX

enfin, déclarez votre clé dans le fichier de conf devsripts /etc/devscripts.conf ainsi :  

	DEBSIGN_KEYID=XXXXXXXX


*pour l'exemple, voici la construction détaillée du [live-usb-creator](https://handylinux.org/wiki/doku.php/fr/tools#liveusbcreator) de thuban et coyotus.*

mise en place de l'environnement de construction
------------------------------------------------
- création du dossier de dev pour le paquet. vous pouvez utiliser le nom que vous voulez.  
`mkdir /handydeb/liveUSBcreator`
- création du dossier source pour le paquet. le dossier source ne doit contenir que des lettres minuscules ou des chiffres.[a-z/0-9]  
`mkdir /handydeb/liveUSBcreator/liveusbcreator-0.1`
- copie du script dans le dossier des sources. le script ne doit pas contenir d'extension et peut se nommer comme vous voulez.  
`cp handy-live-usb-creator /handydeb/liveUSBcreator/liveusbcreator-0.1/liveUSBcreator`
- tour de magie dh_make : cette commande passée dans le dossier des sources, permet de créer l'architecture des fichiers Debian normés.  
c'est une commande interactive qui vous posera une question pour le type de paquet, et une autre pour confirmer les informations récoltées. vous pourez modifier les fichiers créés ultérieurement.  
`cd /handydeb/liveUSBcreator/liveusbcreator-0.1`  
`dh_make --createorig`

retour terminal commenté :  

		dh_make --createorig ## pour créer l'archive source en même temps que les fichiers Debian normés.

		Type of package: single binary, indep binary, multiple binary, library, kernel module, kernel patch?
		[s/i/m/l/k/n] s  ## je réponds 's' car c'est un paquet 'single'
		
		Maintainer name  : arnault perret ## le nom qui sera indiqué dans les fichers Debian
		Email-Address    : arpinux@member.fsf.org  ## l'adresse mail du mainteneur
		Date             : Wed, 06 Jan 2016 22:15:56 +0100
		Package Name     : liveusbcreator  ## nom officiel du paquet
		Version          : 0.1 ## version du paquet
		License          : blank
		Type of Package  : Single
		Hit <enter> to confirm: 
		
		Currently there is no top level Makefile. This may require additional tuning.
		Done. Please edit the files in the debian/ subdirectory now. You should also
		check that the liveusbcreator Makefiles install into $DESTDIR and not in / .

voici l'arborescence créée par dh_make dans le dossier des sources /handydeb/liveUSBcreator. tous les fichiers sont pré-remplis :  

	├── liveusbcreator-0.1 ## dossier des sources au format nomdupaquet-version (tiret)
	│   ├── debian  ## dossier des fichiers Debian normés
	│   │   ├── changelog  ## la liste des modifications. ce fichier est créé par dh_make et modifié par 'dch' (voir la section 'modification d'un paquet')
	│   │   ├── compat  ## ne pas toucher, ne pas supprimer
	│   │   ├── control  ## fichier de contrôle Debian, super important, sera détaillé plus bas
	│   │   ├── copyright  ## la licence
	│   │   ├── docs ## optionnel
	│   │   ├── init.d.ex  ## optionnel, ne sert que pour les applications devant être lancées depuis l'init
	│   │   ├── liveusbcreator.cron.d.ex  ## optionnel, ne sert que si l'application doit être exécutée de façon périodique
	│   │   ├── liveusbcreator.default.ex  ## optionnel, options suppplémentaires pour l'init
	│   │   ├── liveusbcreator.doc-base.EX  ## optionnel
	│   │   ├── manpage.1.ex  ## template de manuel au format de base
	│   │   ├── manpage.sgml.ex  ## template de manuel au format sgml
	│   │   ├── manpage.xml.ex  ## template de manuel ua format xml
	│   │   ├── menu.ex  ## optionnel, info à passer à 'menu'
	│   │   ├── postinst.ex  ## optionnel, script qui s'exécute juste après l'installation
	│   │   ├── postrm.ex  ## optionnel, script qui s'exécute juste après la suppression
	│   │   ├── preinst.ex  ## optionnel, script qui s'exécute juste avant l'installation
	│   │   ├── prerm.ex  ## optionnel, script qui s'exécute juste avant la suppression
	│   │   ├── README.Debian  ## optionnel, si tu as des trucs à dire à Debian
	│   │   ├── README.source  ## optionnel, si tu as des trucs à dire à propos des sources
	│   │   ├── rules  ## les règles de compilation du paquet
	│   │   ├── source  ## dossier qui défini le format des sources
	│   │   │   └── format  ## fichier du format des sources. ne pas toucher, ne pas supprimer.
	│   │   └── watch.ex  ## optionnel, permet de surveiller un emplacement distant (jamais servi encore)
	│   └── liveUSBcreator  ## l'application. même si c'est un script, il n'est pas exécutable dans les sources. (perms 644)
	└── liveusbcreator_0.1.orig.tar.xz  ## l'archive des sources au format nomdupaquet_version (underscore)

vous n'utiliserez pas tous ces fichiers, ils sont là à titre d'exemple : vous devrez éditer les fichiers utiles et supprimer les autres.  
/!\ Attention /!\ Debian est ultra pointilleux sur la syntaxe et l'identation. le passage au lintian (outil de vérification des paquets Debian) vous aidera à corriger les petites erreurs.

quand vous aurez l'habitude, vous n'utiliserez plus la commande 'dh_make' car peu de fichiers sont indispensables pour construire un paquet.  
on va en supprimer pas mal tout de suite. mais on va en ajouter aussi, histoire de mettre un peu de GUI dans cette apllication ;)

voici à quoi ressemblent les sources avant l'édition des fichiers :  

	├── liveusbcreator-0.1
	│   ├── debian
	│   │   ├── changelog
	│   │   ├── compat
	│   │   ├── control
	│   │   ├── copyright
	│   │   ├── rules
	│   │   └── source
	│   │       └── format
	│   ├── liveUSBcreator
	│   ├── liveUSBcreator.desktop ## ajout d'un lanceur d'application qui sera placé dans /usr/share/applications
	│   ├── liveUSBcreator.png ## ajout d'une icône pour l'application qui sera placée dans /usr/share/icons
	│   ├── Makefile ## ajout d'un Makefile qui gère l'installation et la suppression du paquet
	│   └── manpage.1 ## déplacement du manuel au format standard
	└── liveusbcreator_0.1.orig.tar.xz

contenu détaillé des sources
----------------------------
voici le détail des fichiers inclus dans le dossier des sources du paquet.  
note : comme nous allons les éditer, l'archive 'liveusbcreator_0.1.orig.tar.xz' ne sera plus valable (nous allons la refaire après).  

#### changelog  [cf](https://www.debian.org/doc/manuals/maint-guide/dreq.fr.html#changelog)

	liveusbcreator (0.1-1) unstable; urgency=low
	
	  * Première mise en paquet Debian
	
	 -- arnault perret <arpinux@member.fsf.org>  Wed, 06 Jan 2016 22:15:56 +0100

ce fichier a été généré automatiquement avec les infos données lors du 'dh_make'. vous devez la remplir pour notifier la liste des modifications en respectant la syntaxe :  
ce fichier sera modifié à chaque utilisation de la commande 'dch' que nous détaillerons plus tard dans la section 'Modification d'un paquet".

+ pas d'espace pour la première ligne, 'nomdupaquet' (version_du_paquet sous la forme n°upstream-n°revision) 'distribution'(ici je colle 'unstable' car les paquets peuvent aller dans plusieurs distributions -stable,jessie..-); 'urgence de la mise à jour' (si il y a urgence, c'est que ça déconne :P)
+ une ligne vide
+ liste des modifications avec 2 espaces devant, un '*', un espace et la modification (pas trop long please)
+ une ligne vide
+ nom et mail du mainteneur + date avec un espace devant (généré automatiquement)
+ une ligne vide

#### control [cf](https://www.debian.org/doc/manuals/maint-guide/dreq.fr.html#control)

	Source: liveusbcreator
	Section: x11
	Priority: optional
	Maintainer: arnault perret <arpinux@member.fsf.org>
	Build-Depends: debhelper (>= 9)
	Standards-Version: 3.9.6
	Homepage: https://handylinux.org/wiki/doku.php/fr/tools#liveusbcreator
	
	Package: liveusbcreator
	Architecture: all
	Depends: ${misc:Depends}, zenity, coreutils, pv
	Description: Outil de création de live USB
	liveUSBcreator vous permet de transférer facilement 
	une image ISO sur un support amovible USB.
	.
	liveUSBcreator est une interface graphique pour dd.

explication des options possibles (certaines ne sont pas utilisées dans notre exemple)  

+ Source : le nom du paquet source utilisé pour la construction
+ Section : la section de l'application (x11=graphique, bureau, DE)  
options possibles : admin, cli-mono, comm, database, debug, devel, doc, editors, education, electronics, embedded, fonts, games, gnome, gnu-r, gnustep, graphics, hamradio, haskell, httpd, interpreters, introspection, java, kde, kernel, libdevel, libs, lisp, localization, mail, math, metapackages, misc, net, news, ocaml, oldlibs, otherosfs, perl, php, python, ruby, science, shells, sound, tasks, tex, text, utils, vcs, video, web, x11, xfce, zope.
+ Priority : priorité de l'application. indispensable ou pas ?  
options possibles : required, important, standard, optional, extra.
+ Mainteneur : celui qui fait le paquet, peut être différent de l'auteur de l'application (renseigné dans le copyright)
+ Build-Depends : les dépendances indispensables à la construction du paquet
+ Standards-Version : n° de version en cours pour l'outil de construction
+ Homepage : la page d'accueil de l'application si pertinent
+ Package : nom du paquet
+ Architecture : any=arch de la machine qui construit, all=pour toute architecture.
+ Depends : les paquets dépendants indispensables séparés par une virgule et un espace. '${misc:Depends}' permet de couvrir les dépendances courantes genre bash :P
+ Recommends : les paquets dépendants recommendés. sont généralement traités comme les 'Depends', sauf sur HandyLinux, qui n'installe pas les 'Recommends' pour alléger le système
+ Conflicts : les paquets en conflit. un message d'avertissement sera affiché et il appartient à l'utilisateur d'accepter ou non l'installation (et donc la suppression des paquets en conflit)
+ Replaces : même issue que pour 'Conflit', mais permet de gérér la double-dépendance. lorsqu'un paquet est dépendant de `foo1 | foo2` cad foo1 ou foo2, et si foo1 'Replace' foo2, le remplacemeent sera automatique.
+ Suggests : les paquets suggérés, pour info :)
+ Description : description courte du paquet (ce qui sera affiché en premier avec un `apt-cache search`)  
suit ensuite la description longue ou détaillée, identé d'un espace. le '.' sert à marquer un saut de ligne.

#### copyright [cf](https://www.debian.org/doc/manuals/maint-guide/dreq.fr.html#copyright)

	Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
	Upstream-Name: liveusbcreator
	Source: https://handylinux.org
	
	Files: *
	Copyright: 2016 Xavier Cartron <thuban@yeuxdelibad.net>
	           2016 coyotus <contact@handylinux.org>
	           2016 arnault perret <arpinux@member.fsf.org>
	License: GPL-2+
	 This package is free software; you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation; either version 2 of the License, or
	 (at your option) any later version.
	 .
	 This package is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.
	 .
	 You should have received a copy of the GNU General Public License
	 along with this program. If not, see <http://www.gnu.org/licenses/>
	 .
	 On Debian systems, the complete text of the GNU General
	 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

explicite :P ... faut juste respecter les espaces...

#### liveUSBcreator.desktop
le lanceur d'application qui sera copié par le Makefile dans /usr/share/applications

	[Desktop Entry]
	Type=Application
	Encoding=UTF-8
	Name=live USB creator
	GenericName=liveUSBcreator
	Comment=Live USB creation tool
	Comment[fr]=Outil de création de live USB
	Icon=liveUSBcreator
	Exec=/usr/bin/liveUSBcreator
	Terminal=false
	StartupNotify=false
	Categories=Application;Utility;System;

#### liveUSBcreator.1
le manuel qui sera copié par le Makefile dans /usr/share/man/man1

	.\"                                      Hey, EMACS: -*- nroff -*-
	.\" (C) Copyright 2016 arnault perret <arpinux@member.fsf.org>,
	.\"
	.\" First parameter, NAME, should be all caps
	.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
	.\" other parameters are allowed: see man(7), man(1)
	.TH LIVEUSBCREATOR 1 "January  6, 2016"
	.\" Please adjust this date whenever revising the manpage.
	.\"
	.\" Some roff macros, for reference:
	.\" .nh        disable hyphenation
	.\" .hy        enable hyphenation
	.\" .ad l      left justify
	.\" .ad b      justify to both left and right margins
	.\" .nf        disable filling
	.\" .fi        enable filling
	.\" .br        insert line break
	.\" .sp <n>    insert n+1 empty lines
	.\" for manpage-specific macros, see man(7)
	.SH NAME
	liveusbcreator \- utilitaire de création de live USB
	.SH SYNOPSIS
	.B liveusbcreator
	.RI [ options ] " files" ...
	.br
	.SH DESCRIPTION
	.PP
	.\" TeX users may be more comfortable with the \fB<whatever>\fP and
	.\" \fI<whatever>\fP escape sequences to invode bold face and italics,
	.\" respectively.
	\fBliveusbcreator\fP vous permet de transférer facilement une image de type ISO sur un support amovible USB
	.SH OPTIONS
	Vous pouvez spécifier l'image ISO et le /dev de la clé USB en arguments
	.TP
	.B liveUSBcreator /path_to_ISO /dev/sdx
	va transférer l'ISO sur /dev/sdx
	.SH SEE ALSO
	.BR dd (1),
	.br

et le résultat lors du `man liveUSBcreator`

	LIVEUSBCREATOR(1)                        General Commands Manual                       LIVEUSBCREATOR(1)
	
	
	
	NAME
	        liveusbcreator - utilitaire de création de live USB
	
	SYNOPSIS
	        liveusbcreator [options] files...
	
	DESCRIPTION
	        liveusbcreator vous permet de transférer facilement une image de type ISO sur un support amovible USB
	
	OPTIONS
	        Vous pouvez spécifier l'image ISO et le /dev de la clé USB en arguments
	
	        liveUSBcreator /path_to_ISO /dev/sdx
	        va transférer l'ISO sur /dev/sdx
	
	SEE ALSO
	        dd(1)

#### Makefile
enfin... le fichier qui fait...

	# liveUSBcreator makefile arnault perret <arpinux@member.fsf.org>
	
	DESTDIR =
	BINDIR = /usr/bin
	APPDIR = /usr/share/applications
	ICONDIR = /usr/share/icons
	MANDIR = /usr/share/man/man1
	
	help:
		@echo "Usage: as root"
		@echo "make install  : installs application"
		@echo "make uninstall: uninstalls application"
	
	install:
		install -d -m 755 -o root -g root $(DESTDIR)$(BINDIR)
		install -d -m 755 -o root -g root $(DESTDIR)$(APPDIR)
		install -d -m 755 -o root -g root $(DESTDIR)$(ICONDIR)
		install -d -m 755 -o root -g root $(DESTDIR)$(MANDIR)
		install -m 755 -o root -g root liveUSBcreator $(DESTDIR)$(BINDIR)
		install -m 644 -o root -g root liveUSBcreator.1 $(DESTDIR)$(MANDIR)
		install -m 644 -o root -g root liveUSBcreator.png $(DESTDIR)$(ICONDIR)
		install -m 644 -o root -g root liveUSBcreator.desktop $(DESTDIR)$(APPDIR)
	
	uninstall:
		rm $(DESTDIR)$(BINDIR)/liveUSBcreator
		rm $(DESTDIR)$(APPDIR)/liveUSBcreator.desktop
		rm $(DESTDIR)$(MANDIR)/liveUSBcreator.1
		rm ${DESTDIR}${ICONDIR}/liveUSBcreator.png

correspondance des commandes du Makefile :  
`install -d -m 755 -o root -g root $(DESTDIR)$(BINDIR)` == `mkdir -p /usr/bin ; chmod 755 /usr/bin ; chown root:root /usr/bin`  
note : vous pouvez aussi utiliser les commandes classiques (cp, chown, chmod, rm...)  

#### premier test complet
Une fois tous vos fichiers modifiés selon vos besoins et ceux du paquet, il est temps de le tester. simplement avec une installation locale immédiate :D  
`sudo make install`  
qui va vous permettre de visualiser le man, vérifier le bon fonctionnement, vérifier le lanceur et ajuster l'icône ;)  
`sudo make uninstall` pour nettoyer bien sûr.

construction du paquet
----------------------

la construction du paquet se fait avec la commande `debuild` qui est en fait un alias pour les outils de construction 'dpkg'. cependant, il faut que l'archive des sources corresponde au dossier des sources. ce qui n'est plus le cas dans notre exemple car nous avons édité les fichiers normés Debian, ajouté le lanceur etc. bref, il faut recompresser les sources. le format par défaut est 'nomdupaquet_version.orig.tar.xz' pour des sources 'nomdupaquet-version' . les mêmes sources servent pour les révisions (0.1-1; 0.1-2, 0.1-3 ...). des sources différentes seront utilisées lors du changement d'upstream (0.2 > 0.2-1, 0.2-2 ...)

donc depuis le dossier de developpement (liveUSBcreator) :  

- compression des sources :  
`tar cvJf liveusbcreator_0.1.orig.tar.xz liveusbcreator-0.1/`
- construction du paquet depuis les sources :  
`cd liveusbcreator-0.1/ && debuild`

retour terminal :  

	debuild
	dpkg-buildpackage -rfakeroot -D -us -uc
	dpkg-buildpackage: source package liveusbcreator
	dpkg-buildpackage: source version 0.1-1
	dpkg-buildpackage: source distribution unstable
	dpkg-buildpackage: source changed by arnault perret <arpinux@member.fsf.org>
	 dpkg-source --before-build liveusbcreator-0.1
	dpkg-buildpackage: host architecture amd64
	 fakeroot debian/rules clean
	dh clean 
	    dh_testdir
	    dh_auto_clean
	    dh_clean
	 dpkg-source -b liveusbcreator-0.1
	dpkg-source: info: using source format '3.0 (quilt)'
	dpkg-source: info: building liveusbcreator using existing ./liveusbcreator_0.1.orig.tar.xz
	dpkg-source: info: building liveusbcreator in liveusbcreator_0.1-1.debian.tar.xz
	dpkg-source: info: building liveusbcreator in liveusbcreator_0.1-1.dsc
	 debian/rules build
	dh build 
	    dh_testdir
	    dh_auto_configure
	    dh_auto_build
	make[1]: Entering directory '/home/arp/debdev/liveUSBcreator/liveusbcreator-0.1'
	Usage: as root
	make install  : installs application
	make uninstall: uninstalls application
	make[1]: Leaving directory '/home/arp/debdev/liveUSBcreator/liveusbcreator-0.1'
	    dh_auto_test
	 fakeroot debian/rules binary
	dh binary 
	    dh_testroot
	    dh_prep
	    dh_auto_install
	make[1]: Entering directory '/home/arp/debdev/liveUSBcreator/liveusbcreator-0.1'
	install -d -m 755 -o root -g root /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/bin
	install -d -m 755 -o root -g root /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/share/applications
	install -d -m 755 -o root -g root /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/share/icons
	install -d -m 755 -o root -g root /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/share/man/man1
	install -m 755 -o root -g root liveUSBcreator /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/bin
	install -m 644 -o root -g root liveUSBcreator.1 /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/share/man/man1
	install -m 644 -o root -g root liveUSBcreator.png /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/share/icons
	install -m 644 -o root -g root liveUSBcreator.desktop /home/arp/debdev/liveUSBcreator/liveusbcreator-0.1/debian/liveusbcreator/usr/share/applications
	make[1]: Leaving directory '/home/arp/debdev/liveUSBcreator/liveusbcreator-0.1'
	    dh_installdocs
	    dh_installchangelogs
	    dh_installman
	    dh_icons
	    dh_perl
	    dh_link
	    dh_compress
	    dh_fixperms
	    dh_installdeb
	    dh_gencontrol
	dpkg-gencontrol: warning: File::FcntlLock not available; using flock which is not NFS-safe
	    dh_md5sums
	    dh_builddeb
	dpkg-deb: building package 'liveusbcreator' in '../liveusbcreator_0.1-1_all.deb'.
	 dpkg-genchanges  >../liveusbcreator_0.1-1_amd64.changes
	dpkg-genchanges: including full source code in upload
	 dpkg-source --after-build liveusbcreator-0.1
	dpkg-buildpackage: full upload (original source is included)
	Now running lintian...
	W: liveusbcreator: new-package-should-close-itp-bug
	Finished running lintian.
	Now signing changes and any dsc files...
	 signfile liveusbcreator_0.1-1.dsc BE441FCF
	
	You need a passphrase to unlock the secret key for
	user: "arnault perret (arpinux) <arpinux@member.fsf.org>"
	2048-bit RSA key, ID BE441FCF, created 2014-03-27
	
	    
	 signfile liveusbcreator_0.1-1_amd64.changes BE441FCF
	
	You need a passphrase to unlock the secret key for
	user: "arnault perret (arpinux) <arpinux@member.fsf.org>"
	2048-bit RSA key, ID BE441FCF, created 2014-03-27
	
	    
	Successfully signed dsc and changes files

`debuild` m'a demandé le mot de passe pour la clé BE441FCF. si vous n'avez pas configuré votre clé comme expliqué en début de howto, les fichiers ne seront pas signés.  
vous pourrez toutefois les signer lors de l'intégration dans les dépôts grâce à reprepro.

résultat de la commande `debuild` : création du paquet et des fichiers pour les dépôts  

	├── liveusbcreator-0.1
	│   ├── debian
	│   │   ├── changelog
	│   │   ├── compat
	│   │   ├── control
	│   │   ├── copyright
	│   │   ├── files
	│   │   ├── liveusbcreator ## dossier reproduisant l'architecture du paquet et des fichiers installés
	│   │   │   ├── DEBIAN
	│   │   │   │   ├── control
	│   │   │   │   ├── md5sums
	│   │   │   │   ├── postinst
	│   │   │   │   └── postrm
	│   │   │   └── usr
	│   │   │       ├── bin
	│   │   │       │   └── liveUSBcreator
	│   │   │       └── share
	│   │   │           ├── applications
	│   │   │           │   └── liveUSBcreator.desktop
	│   │   │           ├── doc
	│   │   │           │   └── liveusbcreator
	│   │   │           │       ├── changelog.Debian.gz
	│   │   │           │       └── copyright
	│   │   │           ├── icons
	│   │   │           │   └── liveUSBcreator.png
	│   │   │           └── man
	│   │   │               └── man1
	│   │   │                   └── liveUSBcreator.1.gz
	│   │   ├── liveusbcreator.debhelper.log
	│   │   ├── liveusbcreator.postinst.debhelper
	│   │   ├── liveusbcreator.postrm.debhelper
	│   │   ├── liveusbcreator.substvars
	│   │   ├── rules
	│   │   └── source
	│   │       └── format
	│   ├── liveUSBcreator
	│   ├── liveUSBcreator.1
	│   ├── liveUSBcreator.desktop
	│   ├── liveUSBcreator.png
	│   └── Makefile
	├── liveusbcreator_0.1-1_all.deb  ## le paquet compilé
	├── liveusbcreator_0.1-1_amd64.build  ## les logs du build (le retour terminal)
	├── liveusbcreator_0.1-1_amd64.changes  ## description du paquet, spécificités, changelog, sommes de contrôle et signatures
	├── liveusbcreator_0.1-1.debian.tar.xz  ## archive des fichiers Debian
	├── liveusbcreator_0.1-1.dsc  ## fichier de description du paquet
	└── liveusbcreator_0.1.orig.tar.xz  ## sources du paquet

on termine avec un `debuild clean` qui permet de nettoyer le dossier des sources et de revenir à l'environnement de dev de départ.

remarques : `debuild` lance la commande `lintian` qui est l'outil de vérification des paquets Debian. dans cet exemple, lintian affiche un **W**arning, ce qui n'est pas grave en soi (là c'est parce que le premier commit doit cloturer un bug ... jamais compris pourquoi :) ).  
prenez le temps de lire les avertissements et les erreurs lintian qui vous aideront à construire des paquets aux normes Debian, facilitant ainsi grandement la gestion des paquets grâce à apt ou aptitude :)

le paquet est désormais prêt à être intégré dans les dépôts. mais il est assez rare que tout fonctionne parfaitement du premier coup, qu'il n'y ai rien à revoir... donc avant d'intégrer, on va de suite voir comment fonctionne la modification d'un paquet depuis les sources.

Modification d'un paquet
========================
vous pouvez modifier un paquet avant ou après intégration. dans le second cas, c'est une mise à jour.

#### modifications des sources
éditer les fichiers directement dans les sources, fixez les bugs, appliquez vos améliorations...

#### patcher les sources
afin de conserver les sources propres et permettre une gestion fine des modifications, nous allons utiliser la commande `dpkg-source` depuis le dossier des sources :  
`cd liveusbcreator-0.1 && dpkg-source --commit`  
cette commande va lister les modifications comme le fait le protocole git lors d'un `git add --all ; git commit -a`.  
vous devez 'nommer' le patch puis vous serez redirigé vers le fichier de description complet du patch.  
retour terminal

	dpkg-source --commit
	dpkg-source: info: local changes detected, the modified files are:
	 liveusbcreator-0.1/liveUSBcreator.1
	 liveusbcreator-0.1/liveUSBcreator.desktop
	Enter the desired patch name: minorfix1

	Description: simple mise en forme
	 .
	 liveusbcreator (0.1-1) unstable; urgency=low
	 .
	    * Première mise en paquet Debian
	Author: arnault perret <arpinux@member.fsf.org>
	
	---
	The information above should follow the Patch Tagging Guidelines, please
	checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
	are templates for supplementary fields that you might want to add:
	
	Origin: <vendor|upstream|other>, <url of original patch>
	Bug: <url in upstream bugtracker>
	Bug-Debian: https://bugs.debian.org/<bugnumber>
	Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
	Forwarded: <no|not-needed|url proving that it has been forwarded>
	Reviewed-By: <name and email of someone who approved the patch>
	Last-Update: <YYYY-MM-DD>
	
	--- liveusbcreator-0.1.orig/liveUSBcreator.1
	+++ liveusbcreator-0.1/liveUSBcreator.1
	@@ -18,10 +18,10 @@
	 .\" .sp <n>    insert n+1 empty lines
	 .\" for manpage-specific macros, see man(7)
	 .SH NAME
	-liveusbcreator \- utilitaire de création de live USB
	+liveusbcreator \- utilitaire de transfert pour live USB
	 .SH SYNOPSIS
	 .B liveusbcreator
	-.RI [ options ] " files" ...
	+.RI "image.iso" "/path_to_usb_device" ...
	 .br
	 .SH DESCRIPTION
	 .PP
	--- liveusbcreator-0.1.orig/liveUSBcreator.desktop
	+++ liveusbcreator-0.1/liveUSBcreator.desktop
	@@ -4,7 +4,7 @@ Encoding=UTF-8
	 Name=live USB creator
	 GenericName=liveUSBcreator
	 Comment=Live USB creation tool
	-Comment[fr]=Outil de création de live USB
	+Comment[fr]=Outil de transfert pour live USB
	 Icon=liveUSBcreator
	 Exec=/usr/bin/liveUSBcreator
	 Terminal=false

ce fichier s'ouvrira dans votre éditeur par défaut. une fois sauvegardé, vous reprendrez la main sur le terminal et pourrez continuer la procédure de modification.

#### déclarer les changements
une fois le patch décrit, vous devez lister les modifications dans le changelog. c'est possible '@ la main' mais Debian a tout prévu et encore une fois, peut pré-remplir votre déclaration grâce à la commande `dch` passée dans le dossier des sources. cette commande va ouvrir le fichier changelog avec l'éditeur par défaut. il vous reste à préciser la distribution (généralement, je colle 'unstable' car un paquet peut aller dans plusieurs distros).  
  `dch`

	liveusbcreator (0.1-2) unstable; urgency=medium
	
	  * mise en forme
	
	 -- arnault perret <arpinux@member.fsf.org>  Thu, 07 Jan 2016 17:12:26 +0100
	
	liveusbcreator (0.1-1) unstable; urgency=low
	
	  * Première mise en paquet Debian
	
	 -- arnault perret <arpinux@member.fsf.org>  Wed, 06 Jan 2016 22:15:56 +0100

notez que le n° de révision (-2) a été édité automatiquement et vous n'avez pas à le modifier, sauf si vous procédez à un changement de version (0.1 > 0.2). dans ce cas, vous devrez éditer le n° de version (0.2-1) et le dossier sera renommé par 'dch'.

#### construction du paquet
de la même façon que la première fois, on construit avec `debuild` et on nettoie avec `debuild clean`.

qu'est-ce-qui a changé ?  

+ ce que vous avez modifié (c'est évident)
+ création automatique d'un dossier /debian/patches qui contient une liste des patches et les descriptions que vous avez édité
+ un dossier /.pc contenant les patches eux-mêmes ainsi que les fichiers normés Debian correspondants.

l'architecture du dossier principal après toutes les opérations :

	├── liveusbcreator-0.1
	│   ├── debian
	│   │   ├── changelog
	│   │   ├── compat
	│   │   ├── control
	│   │   ├── copyright
	│   │   ├── patches
	│   │   │   ├── minorfix1
	│   │   │   └── series
	│   │   ├── rules
	│   │   └── source
	│   │       └── format
	│   ├── liveUSBcreator
	│   ├── liveUSBcreator.1
	│   ├── liveUSBcreator.desktop
	│   ├── liveUSBcreator.png
	│   ├── Makefile
	│   └── .pc
	│       ├── applied-patches
	│       ├── minorfix1
	│       │   ├── liveUSBcreator.1
	│       │   └── liveUSBcreator.desktop
	│       ├── .quilt_patches
	│       ├── .quilt_series
	│       └── .version
	├── liveusbcreator_0.1-1_all.deb
	├── liveusbcreator_0.1-1_amd64.build
	├── liveusbcreator_0.1-1_amd64.changes
	├── liveusbcreator_0.1-1.debian.tar.xz
	├── liveusbcreator_0.1-1.dsc
	├── liveusbcreator_0.1-2_all.deb
	├── liveusbcreator_0.1-2_amd64.build
	├── liveusbcreator_0.1-2_amd64.changes
	├── liveusbcreator_0.1-2.debian.tar.xz
	├── liveusbcreator_0.1-2.dsc
	└── liveusbcreator_0.1.orig.tar.xz

Intégration dans le dépôt
=========================
une fois le(s) paquet(s) modifiés, il faut les intégrer aux dépôts.  
HandyLinux dispose de trois dépôts : stable, compiz et jessie :
  
- les dépôts "stable" (j'avais donné ce nom au début, je ne savais pas que ça irait si loin) et "compiz" sont pour HandyLinux-1.x et sont compatibles avec Debian Wheezy.
- le dépôt "jessie" est pour HandyLinux-2.x et est compatible avec Debian Jessie.

le choix du dépôt d'intégration s'effectue en argument de la commande reprepro (détaillée plus tard)

structure du dépôt Debian
-------------------------
tous les fichiers sont automatiquement mis à jour si nécessaire lors de l'utilisation de reprepro (merci Debian). Pour mettre en place un dépôt, il faut un dossier 'debian/conf' avec un fichier 'distributions' qui liste les distros supportées et leurs spécificités :  
**http://repo.handylinux.org/debian/conf/distributions** : fichier de configuration des dépôts HandyLinux

	Origin: handylinux.org
	Label: handylinux repository
	Codename: stable
	Architectures: i386 amd64 source
	Components: main
	Description: HandyLinux main repository
	SignWith: BE441FCF
	
	Origin: handylinux.org
	Label: handylinux repository
	Codename: compiz
	Architectures: i386 amd64 source
	Components: main
	Description: HandyLinux compiz repository
	SignWith: BE441FCF
	
	Origin: handylinux.org
	Label: handylinux repository
	Codename: jessie
	Architectures: i386 amd64 source
	Components: main
	Description: HandyLinux jessie repository
	SignWith: BE441FCF

voici la structure de base : repo/. [accessible ici](http://repo.handylinux.org)  

	├── debian : dossier contenant les paquets, les sources et la configuration  
	│   ├── conf  
	│   │   └── distributions : fichier de configuration des dépôts. il contient la liste des dépôts et leurs infos respectives  
	│   ├── db : dossier des "db" ... listing des infos, paquets, md5 etc... se met à jour automatiquemeent avec reprepro.  
	│   │   ├── checksums.db  
	│   │   ├── contents.cache.db  
	│   │   ├── packages.db  
	│   │   ├── references.db  
	│   │   ├── release.caches.db  
	│   │   └── version  
	│   ├── dists : dossier des distributions qui liste les infos de chaque dépôt  
	│   │   ├── compiz  
	│   │   │   └── ...  
	│   │   ├── jessie  
	│   │   │   ├── InRelease  
	│   │   │   ├── main  
	│   │   │   │   ├── binary-amd64  
	│   │   │   │   │   ├── Packages  
	│   │   │   │   │   ├── Packages.gz  
	│   │   │   │   │   └── Release  
	│   │   │   │   ├── binary-i386  
	│   │   │   │   │   ├── Packages  
	│   │   │   │   │   ├── Packages.gz  
	│   │   │   │   │   └── Release  
	│   │   │   │   └── source  
	│   │   │   │       ├── Release  
	│   │   │   │       └── Sources.gz  
	│   │   │   ├── Release  
	│   │   │   └── Release.gpg  
	│   │   └── stable  
	│   │       └── ...  
	│   └── pool : dossier des paquets  
	│       └── main  
	│           ├── b  
	│           │   └── btshare-hweb  
	│           │       ├── btshare-hweb_0.2.7_all.deb  
	│           │       ├── btshare-hweb_0.2.7.debian.tar.gz  
	│           │       ├── btshare-hweb_0.2.7.dsc  
	│           │       └── btshare-hweb_0.2.7.orig.tar.gz  
	│           ├── c  
	│           │   └── ...  
	│           └── x  
	├── handylinux.list : le sources.list complet handylinux, avec tous les dépôts  
	├── key  
	│   └── handylinux.gpg.key : clé d'authentification pour les dépôts. pour l'instant calée sur arpinux@member.fsf.org  
	├── README : description publique des dépôts et rappel des licences.  
	└── sources.list : le sources.list Debian de la release actuelle (jessie au moment d'HandyLinux-2.x)  


utilsation de reprepro
----------------------
reprepro se charge de mettre à jour tous les fichiers contenant le listing des paquets et leurs versions et de supprimer les paquets obsolètes. comme d'hab, cette méthode ne présente pas toutes les options de reprepro... un ptit `man reprepro` vous éclairera sur les nombreuses possibilités de cet outil.  
il se lance depuis le dossier 'debian' du dépôt concerné.  

détails de la commande pour intégrer notre nouvel outil dans les dépôts "jessie" d'HandyLinux :  

	reprepro --ignore=wrongdistribution include jessie ../../liveUSBcreator/liveusbcreator_0.1-2_amd64.changes
	            │                         │         │          │
	            │                         │         │          └──adresse (relative) du fichiers *.changes
	            │                         │         └──distribution dans laquelle le paquet devra être intégré
	            │                         └──argument d'intégration qui prend en charge tous les fichiers (.changes, .dsc, .)
	            └──cette option pour empêcher une erreur de reprepro.
	               le fichier de description du paquet indique 'unstable' au lieu de 'jessie' (car les paquets vont parfois dans 'stable' aussi.).

note : l'argument 'include' permet de prendre en charge la totalité des fichiers. il a cependant besoin de l'archive d'origine pour intégrer les fichiers.  
dans notre exemple, comme nous voulions intégrer directement la version '0.1-2', le premier paquet, l'origine, n'a pas été intégrée dans le dépôt. je l'ai donc fait à la main (copie du .orig.tar.xz dans le dossier /debian/pool/main/l/liveusbcreator) afin qu'elle soit reconnue par 'include .changes'

pour n'intégrer que le paquet compilé, vous pouvez utiliser l'argument 'includedeb' (ce que j'ai fait pour les paquets compiz pré-compilés).  

Mise en ligne du dépôt
======================

comme le dépôt est en local, il suffit de synchroniser le dossier '/repo' sur le serveur :

- avec un client graphique ftp comme filezilla (option : écraser les données si plus récentes ou pas la même taille)
- avec un client graphique de synchronisation comme unison
- avec un client CLI, mais j'ai jamais essayé, filezilla me convient très bien pour un petit dépôt.

note: si la mise à jour n'est pas urgente, mater les logs du serveur pour connaître les meilleures heures d'upload ;)

------------------------------------------------------------------------

TODO :  

- mettre en place un handylinux-keyring pour que plusieurs personnes (et donc plusieurs clés) puissent travailler sur les dépôts.  
alternative : ajouter à la clé actuelle, une clé associée à 'dev@handylinux.org' et partager la clé et le mot de passe entre développeurs handylinux
- ...


------------------------------------------------------------------------

des questions ?
---------------
@devs : posez vos questions ici ou en commentaires, ça étoffera le tuto :P
